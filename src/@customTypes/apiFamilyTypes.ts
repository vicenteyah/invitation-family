
interface familyTree{
    name: string;
    lastName: string;
}



export interface IFamily{
    id: number;
    query: string;
    familyId: string;
    mainGuest: string;
    familyTree: familyTree[];
}