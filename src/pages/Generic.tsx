import { WithNavigation } from "../components/HOC/withNavigation";
import SectionHero from "../components/molecules/SectionHero";
import ScheduleProgram from "../components/molecules/SectionHero/Schedule";
import CardWrapper from "../components/molecules/Card";
import Home from "../assets/img/casa.jpg";

import Location from "../components/molecules/SectionHero/Location";

export const Generic = () => {
  return (
    <WithNavigation>
      <SectionHero>
        <CardWrapper title="Celebremos los 100 años" subject="Maria Vicenta">
          <img src={Home} alt="casa" />
          <div className="card-description">
            <a href="#hello" rel="noopener noreferrer">
              {" "}
              (Sábado) 10 Septiembre del 2022{" "}
            </a>
          </div>
        </CardWrapper>
      </SectionHero>
      <ScheduleProgram
        title="Programa de Cumpleaños"
        date="Sábado 10 Septiembre del 2022"
      >
        <ul>
          <li>
            Foto familiar a las 6:30pm en <br /> la iglesia de la Ermita de
            Santa isabel <br />
            <strong>No Faltes!</strong>
          </li>
          <li>Misa: 7:00pm</li>
          <li>
            Código de vestimenta: <strong>Regional</strong>
          </li>
          <li>
            Caballeros:{" "}
            <strong>
              Guayabera, filipina, <br />
              camisa de color claro
            </strong>
          </li>
          <li>
            Damas: <strong>Blusa, vestido, hipil o terno.</strong>{" "}
          </li>
          <li>
            Vaqueria a las: <strong>9:00pm</strong>
          </li>
        </ul>
      </ScheduleProgram>
      <Location title="Ubicación del evento">
        <p>
          La dirección de la recepción:{" "}
          <strong>
            Calle 87 #504f entre 64 y 62 col. centro Mérida Yucatán.
          </strong>{" "}
        </p>
        <p>
          <strong>
            Puedes llevar tu propio consumo como botanas, cerveza, etc.
          </strong>
        </p>
      </Location>
    </WithNavigation>
  );
};
