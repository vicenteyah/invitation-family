import { BrowserRouter as Router , useRoutes } from 'react-router-dom';
import { Generic } from "./pages/Generic";
import { Special } from './pages/Special';


const Routers = () => {
  let Routes = useRoutes([
    {path: '/', element: <Generic />},
    {path: '/special', element: <Special />},
  ])
  return Routes;
}

const App = () =>{
  
  return(
    <Router>
      <Routers/>
    </Router>
  )
};

export default App;
