import { IFamily } from "../../@customTypes/apiFamilyTypes";

export const FamilyGuestsList: IFamily[] = [
  {
    id: 1,
    query: "FLD",
    familyId: "Familia Lopez Dzul",
    mainGuest: "Ethel Yamile Dzul Ic",
    familyTree: [
      {
        name: "Gonzalo",
        lastName: "Lopez",
      },
      {
        name: "Ximena",
        lastName: "Lopez Dzul",
      },
      {
        name: "Natalia",
        lastName: "Lopez Dzul",
      },
    ],
  },
  {
    id: 2,
    query: "FDB",
    familyId: "Familia Dzul Balderas",
    mainGuest: "Felipe Dzul Baas",
    familyTree: [
      {
        name: "Rosy",
        lastName: "Balderas",
      },
      {
        name: "Joel",
        lastName: "Dzul Balderas",
      },
    ],
  },
  {
    id: 3,
    query: "FND1",
    familyId: "Familia Noh Dzul",
    mainGuest: "Silvia Noh Dzul",
    familyTree: [
      {
        name: "Mariano",
        lastName: "Noh",
      },
      {
        name: "Isabel",
        lastName: "Dzul Baas",
      },
      {
        name: "Eugenia",
        lastName: "Canul Noh",
      },
    ],
  },
  {
    id: 4,
    query: "FND2",
    familyId: "Familia Noh Dzul",
    mainGuest: "Cinthia G. Noh Dzul",
    familyTree: [
      {
        name: "Francelli",
        lastName: "Dzul Baas",
      },
      {
        name: "Julio",
        lastName: "Noh Dzul",
      },
      {
        name: "Adrian Emmanuel",
        lastName: "Noh Dzul",
      },
    ],
  },
  {
    id: 4,
    query: "FNM",
    familyId: "Familia Noh Martinez",
    mainGuest: "Alfredo Noh Dzul",
    familyTree: [
      {
        name: "Angel",
        lastName: "Noh Martinez",
      },
      {
        name: "Juan",
        lastName: "Noh Martinez",
      },
      {
        name: "Christina",
        lastName: "Noh Martinez",
      },
      {
        name: "Karla",
        lastName: "Negroe",
      },
    ],
  },
  {
    id: 5,
    query: "FZD",
    familyId: "Familia Zaldivar Dzul",
    mainGuest: "Maria de la cruz Dzul",
    familyTree: [
      {
        name: "Angelica",
        lastName: "Zaldivar Dzul",
      },
      {
        name: "David Antonio",
        lastName: "Zaldivar Martinez",
      },
    ],
  },
  {
    id: 6,
    query: "FYD",
    familyId: "Familia Yah Dzul",
    mainGuest: "Marcela Yah Dzul ",
    familyTree: [
      {
        name: "Eliud",
        lastName: "Yah Dzul",
      },
      {
        name: "Edson",
        lastName: "Yah Dzul",
      },
      {
        name: "Eleuterio",
        lastName: "Yah Canche",
      },
    ],
  },
  {
    id: 7,
    query: "FDI",
    familyId: "Familia Dzul Ic",
    mainGuest: "Pablo Dzul Ic",
    familyTree: [
      {
        name: "Ismael",
        lastName: "Dzul Ic",
      },
    ],
  },
  {
    id: 8,
    query: "FDB2",
    familyId: "Familia Dzul Borges",
    mainGuest: "Luis Dzul Ic",
    familyTree: [
      {
        name: "Andrik",
        lastName: "Dzul Borges",
      },
      {
        name: "Ethel",
        lastName: "Dzul Borges",
      },
      {
        name: "Christian",
        lastName: "Yama",
      },
      {
        name: "Nicole",
        lastName: "Yama Dzul",
      },
    ],
  },
  {
    id: 9,
    query: "VYO",
    familyId: "Vanessa Dzul y Oyuki",
    mainGuest: "Vanessa Dzul Y Oyuki Andrade",
    familyTree: [
      {
        name: "",
        lastName: "",
      },
    ],
  },
  {
    id: 10,
    query: "FND3",
    familyId: "Familia Noh Dominguez",
    mainGuest: "Ramon G. Noh Dzul",
    familyTree: [
      {
        name: "Tania Carmina",
        lastName: "Dominguez",
      },
      {
        name: "Ana",
        lastName: "Dominguez",
      },
    ],
  },
  {
    id: 11,
    query: "FRN",
    familyId: "Familia Ruiz Noh",
    mainGuest: "Claudia Noh Dzul",
    familyTree: [
      {
        name: "Juan",
        lastName: "Ruiz T.",
      },
      {
        name: "Kevin",
        lastName: "Ruiz N.",
      },
      {
        name: "Alejandro",
        lastName: "Ruiz N.",
      },
    ],
  },
  {
    id: 12,
    query: "FZA",
    familyId: "Familia Zaldivar Argaez",
    mainGuest: "David Zaldivar Dzul",
    familyTree: [
      {
        name: "Katia",
        lastName: "Argaez",
      },
      {
        name: "Alison",
        lastName: "Zaldivar Argaez",
      },
    ],
  },
  {
    id: 13,
    query: "FCD",
    familyId: "Familia Cocom Dzul",
    mainGuest: "Marisol Dzul Balderas",
    familyTree: [
      {
        name: "Miguel",
        lastName: "Cocom",
      },
      {
        name: "Mateo",
        lastName: "Cocom Dzul",
      },
      {
        name: "Santiago",
        lastName: "Cocom Dzul",
      },
    ],
  },
  {
    id: 14,
    query: "FEN",
    familyId: "Familia EK Noh",
    mainGuest: "Leticia Noh Dzul",
    familyTree: [
      {
        name: "Juan",
        lastName: "Ek",
      },
      {
        name: "Emmanuel",
        lastName: "Ek Noh",
      },
      {
        name: "Ezequiel",
        lastName: "Ek Noh",
      },
      {
        name: "Elizabeth",
        lastName: "Ek Noh",
      },
    ],
  },
  {
    id: 15,
    query: "FMN",
    familyId: "Familia Moo Noh",
    mainGuest: "Luis Moo Hau",
    familyTree: [
      {
        name: "Jesus Alberto",
        lastName: "Moo Noh",
      },
      {
        name: "Claudia",
        lastName: "Mandujano",
      },
    ],
  },
  {
    id: 16,
    query: "FRD",
    familyId: "Familia Ramirez Dzul",
    mainGuest: "Suemi Dzul Balderas",
    familyTree: [
      {
        name: "Luis",
        lastName: "Ramirez",
      },
      {
        name: "Johnatan",
        lastName: "Ramirez Dzul",
      },
      {
        name: "Mariel",
        lastName: "Ramirez Dzul",
      },
    ],
  },
  {
    id: 17,
    query: "FDP",
    familyId: "Familia Dzul Puch",
    mainGuest: "Familia Dzul Puch",
    familyTree: [
      {
        name: "Hector A.",
        lastName: "Dzul Puch",
      },
      {
        name: "Eduardo",
        lastName: "Dzul Puch",
      },
      {
        name: "Lourdes",
        lastName: "Puch Novelo",
      },
      {
        name: "Ismael",
        lastName: "Dzul Ic",
      },
    ],
  },
];
