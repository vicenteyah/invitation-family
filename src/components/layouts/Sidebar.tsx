import React from "react";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemIcon from "@mui/material/ListItemIcon";
import ListItemText from "@mui/material/ListItemText";

import { Person, Home, Info} from "@mui/icons-material";

interface SidebarProps {
  open?: boolean;
}

export const Sidebar = ({ open }: SidebarProps) => {
  return (
    <List>
      <ListItem style={{ color: "#000000" }} button  href="#home">
        <ListItemIcon>
          <Home />
        </ListItemIcon>
        <ListItemText primary="Bienvendida" />
      </ListItem>
      <ListItem
        style={{ color: "#000000" }}
        button
      >
        <ListItemIcon>
          <Person />
        </ListItemIcon>
        <ListItemText primary="Planeación"  />
      </ListItem>

      <ListItem style={{ color: "#000000" }}  button>
        <ListItemIcon>
          <Info />
        </ListItemIcon>
        <ListItemText primary="Ubicación" />
      </ListItem>
    </List>
  );
};
