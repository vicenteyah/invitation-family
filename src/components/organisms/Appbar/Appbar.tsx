import React from 'react'
import { AppBar } from './AppbarEngine'
import Toolbar from '@mui/material/Toolbar'
import IconButton from '@mui/material/IconButton'
import MenuIcon from '@mui/icons-material/Menu';
import useSound from 'use-sound'
import peregrinaSX from '../../../sounds/peregrina.mp3'

interface NavbarProps {
    open?: boolean;
    handleDrawerOpen?: () => void;
}

export const Navbar = (props:NavbarProps) => {

    const [play] = useSound(peregrinaSX, { volume: 0.25 })

    React.useEffect(() => {
        play()
    }, [play])
    
    return (
        <AppBar style={{backgroundColor:"rgba(152, 123, 102, 0.90)"}} position='fixed' open={props.open}>
            <Toolbar>
                <IconButton
                    color="inherit"
                    aria-label="open drawer"
                    onClick={props.handleDrawerOpen}
                    edge="start"
                    sx={{
                    marginRight: 5,
                        ...(props.open && { display: 'none' }),
                        color : "#000000"
                    }}
                >
                    <MenuIcon />
                </IconButton>

            </Toolbar>
        </AppBar>
    )
}