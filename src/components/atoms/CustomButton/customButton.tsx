
import React from 'react'
import './style.css'

interface CustomButtonProps {
    title: string,
    onClick: () => void,
}


const CustomButton = ({ title, ...props }:CustomButtonProps) => {
    return (
        <button {...props}>
            {title}
        </button>
    )
}

export default CustomButton;