import { Grid, Container } from '@mui/material'

interface SectionContentProps {
    children?: React.ReactNode;
    color?: string;
    justifyContent?:string
    spacing?:number
    direction?: any
    alignItems?: string
    marginTop?:number
    paddingTop?:number
    paddingBottom?:number
    sx?:Object
}

const Section = ({
    children, 
    color, 
    justifyContent,
    spacing ,
    direction,
    alignItems,
    marginTop,
    paddingTop,
    paddingBottom,
    sx
}:SectionContentProps) => {
    return (
        <Container style={{
                marginTop: marginTop,
                paddingTop: paddingTop,
                paddingBottom: paddingBottom,
                paddingRight: 0,
                paddingLeft: 0,
                backgroundColor: color,
                borderRadius: 10,
            }}
        >
            <Grid 
                container 
                justifyContent={justifyContent} 
                spacing={spacing}
                direction={direction}
                alignItems={alignItems}
                sx={sx}
            >
                {children}
            </Grid>
        </Container>
    )
}
export default Section