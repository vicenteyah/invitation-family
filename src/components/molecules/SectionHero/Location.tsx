
import React from 'react'
import './location.css'


interface locationProps {
    title?: string;
    children?: React.ReactNode;
}


const Location = (props:locationProps) => {
    return(
        <section className="location-container">
            <article className="location-article">
              <div className='location-title'>
                <h1>{props.title}</h1>
              </div>
              <div className='location-info'>
                {props.children}
              </div>
              <div className='location-btn'>
                <a href="https://www.google.com.mx/maps/place/C.+87+501Q,+Centro,+97000+Centro,+Yuc./@20.9509628,-89.6289795,19z/data=!3m1!4b1!4m5!3m4!1s0x8f5673d4c8afdc09:0x2b2193a32ad69d15!8m2!3d20.9509615!4d-89.6284323"> ver en maps</a>
              </div>
            </article>
        </section>
    )
}

export default Location