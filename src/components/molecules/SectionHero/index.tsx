import './styles.css'
interface SectionHeroTypes {
    children?: React.ReactNode;
}
const SectionHero = (props:SectionHeroTypes) => {
    return (
        <section id="home" className="container-generic">
            <div className='container-hero'>
                {props.children}
            </div>
        </section>
    )
}

export default SectionHero