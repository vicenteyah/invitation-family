import React from 'react'
import './Schedule.css'
import confetti from '../../../assets/img/headerConfetti.png'
import {IFamily} from '../../../@customTypes/apiFamilyTypes'

interface scheduleProps {
    title?: string;
    date?: string;
    children?: React.ReactNode;
    guest?: IFamily;
}

const ScheduleProgram = (props:scheduleProps) => {
    const tree = props.guest?.familyTree
    

    return(
        <section className="schedule-container">
            <article className="article">
                <div className="article-title"> 
                    <img src={confetti} alt="confeti"/>
                    <h1>{props.title}</h1>
                    <h4>Hola {props.guest?.mainGuest}</h4>
                    <div className="article-guest-complement">
                    <p>Te esperamos junto con tu familia:</p>
                        <ul>
                            {tree?.map((item) => (
                                <li key={item.name}>{item.name} {item.lastName}</li>
                            ))}
                        </ul>
                    </div>
                </div>
                <p>{props.date}</p>
                <div className="article-content">
                    {props.children}
                </div>
            </article>
        </section>  
    )
}

export default ScheduleProgram