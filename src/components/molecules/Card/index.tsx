import './styles.css'

interface cardProps {
    title: string;
    subject: string;
    children?: React.ReactNode;
    guest?: string;
    familyTree?: string;
}

const CardWrapper = ({title,  children,subject, ...rest}:cardProps) =>{
    return(
        <>  
            <div className="card-title">
                <h1>{title} de {subject}</h1>    
            </div>
            <div className="card-content">
                {children}
            </div>
        </>
    )
}

export default CardWrapper